﻿
using UnityEngine;

public class SphereControl : MonoBehaviour
{
    Vector3 _sphereMovementStep = new Vector3(0.05f,0.15f,0);
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += _sphereMovementStep;
        if (this.transform.position.x >= 1.0f || this.transform.position.x <= -1.0f)
        {
            _sphereMovementStep.x *= -1;

        }
        if (this.transform.position.y >= 1.0f || this.transform.position.y <= -1.0f)
        {
            _sphereMovementStep.y *= -1;

        }
        if (this.transform.position.z >= 1.0f || this.transform.position.z <= -1.0f)
        {
           _sphereMovementStep.z *= -1;

        }
        if (Input.GetMouseButtonDown(0))
        {
           GameObject sphere = GameObject.Find("Sphere"); 
           Vector3 spherePosition = sphere.transform.position;
           Vector3 vecToSphereNorm = spherePosition - this.transform.position;
           vecToSphereNorm.Normalize();

           this._sphereMovementStep = vecToSphereNorm/10.0f;
        }
    }
}
